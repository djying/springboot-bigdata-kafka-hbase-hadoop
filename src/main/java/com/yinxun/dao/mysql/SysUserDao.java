package com.yinxun.dao.mysql;

import com.yinxun.entity.SysUserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper
@Component
public interface SysUserDao {
    @Select("SELECT * FROM sys_user")
    public List<SysUserEntity> queryAll();
    
    public List<SysUserEntity> findAll();
}
