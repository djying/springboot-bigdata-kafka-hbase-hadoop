package com.yinxun.config.phoenix;/**
 * @author Administrator
 * @create 2019/4/25.
 */

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/***
 * <p>属性值</p>
 * @ClassName PhoenixProperties.java
 * @author czh
 * @date 2019/4/25 15:19
 * @version 1.0
 */
@Data
@Component
@ConfigurationProperties(prefix = "phoenix.datasource")
public class PhoenixProperties {
    private String driverClassName;
    private String jdbcUrl;
    private String username;
    private String password;
    private String maxPoolSize;
    private String minIdle;
    private String validationTimeout;
    private String idleTimeout;
    private String connectionTestQuery;
}
