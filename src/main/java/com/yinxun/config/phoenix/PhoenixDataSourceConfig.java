package com.yinxun.config.phoenix;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yinxun.config.HikariDataSourceFactory;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Properties;
import java.util.Set;


@Configuration
@MapperScan(basePackages =PhoenixDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "PhoenixSqlSessionFactory")
public class PhoenixDataSourceConfig {
    //mybatis扫描的接口path
    public static final String PACKAGE = "com.yinxun.dao.phoenix.**";

    @Value("${mybatis.config-location}")
    private String mybatisResource;

    @Value("${phoenix.mapperLocations}")
    private String mapperLocations;
    
    @Autowired
    private PhoenixProperties properties;
    
    @Bean(name = "PhoenixDataSource")
//    @ConfigurationProperties(prefix = "spring.datasource.test1")
    public DataSource phoenixDataSource() throws Exception {
        /*ResourceLoader loader = new DefaultResourceLoader();
        InputStream inputStream = loader.getResource(phoenixProperties)
                .getInputStream();
        Properties properties = new Properties();
        
        properties.load(inputStream);
        Set<Object> keys = properties.keySet();
        Properties dsProperties = new Properties();

        for (Object key : keys) {
            if (key.toString().startsWith("phoenix.datasource")) {
                dsProperties.put(key.toString().replace("phoenix.datasource.", ""), properties.get(key));
            }
        }*/
        Properties dsProperties = new Properties();
        JSONObject jsonObject = JSON.parseObject(JSON.toJSONString(properties));
        for (Map.Entry<String, Object> entry : jsonObject.entrySet()) {
            dsProperties.put(entry.getKey().replace("phoenix.datasource.", ""), entry.getValue());
        }

        HikariDataSourceFactory factory = new HikariDataSourceFactory();
        factory.setProperties(dsProperties);
        return factory.getDataSource();
    }

    @Bean(name = "PhoenixSqlSessionFactory")
    public SqlSessionFactory phoenixSqlSessionFactory(
            @Qualifier("PhoenixDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setMapperLocations(new PathMatchingResourcePatternResolver().getResources(mapperLocations));
        ResourceLoader loader = new DefaultResourceLoader();
        factoryBean.setConfigLocation(loader.getResource(mybatisResource));
        factoryBean.setSqlSessionFactoryBuilder(new SqlSessionFactoryBuilder());
        return factoryBean.getObject();
    }

}
