package com.yinxun.controller;


import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yinxun.dao.mysql.SysUserDao;
import com.yinxun.entity.SysUserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * mysql测试控制器
 */
@RequestMapping("mysql")
@RestController
public class MysqlController {

    @Autowired
    private SysUserDao sysUserDao;
    
    @Resource
    private PageHelper pageHelper;

    @RequestMapping("/user/findAll")
    public PageInfo<SysUserEntity> findAll(){
        //先设置分页
        pageHelper.startPage(1,2);
        List<SysUserEntity> users = sysUserDao.findAll();
        PageInfo<SysUserEntity> pageInfo = new PageInfo<SysUserEntity>(users);
        
        return pageInfo;
    }
}
