package com.yinxun.controller;

import com.yinxun.dao.phoenix.UserInfoDao;
import com.yinxun.entity.UserInfo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * phoenix测试控制器
 */
@RequestMapping("phoenix")
@RestController
public class PhoenixController {

    @Autowired
    UserInfoDao userInfoMapper;
    
    @Resource
    private PageHelper phoenixPageHelper;

    @RequestMapping("getAll")
    public List<UserInfo> getAll(){
        return userInfoMapper.getUsers();
    }

    @GetMapping("getById/{id}")
    public UserInfo getById(@PathVariable("id") int id){
        return userInfoMapper.findById(id);
    }

    @GetMapping("page/{pageNum}/{pageSize}")
    public List<UserInfo> page(@PathVariable("pageNum") int pageNum,@PathVariable("pageSize") int pageSize){
        phoenixPageHelper.startPage(pageNum,pageSize);
        List<UserInfo> users = userInfoMapper.findAll();
        PageInfo page=new PageInfo(users);
        return page.getList();
    }

}
