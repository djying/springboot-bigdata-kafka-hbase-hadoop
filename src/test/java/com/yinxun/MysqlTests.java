package com.yinxun;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yinxun.entity.SysUserEntity;
import com.yinxun.dao.mysql.SysUserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MysqlTests {
    @Autowired
    private SysUserDao sysUserDao;

    @Autowired
    private PageHelper pageHelper;

    @Test
    public void contextLoads() {
        List<SysUserEntity> users = sysUserDao.queryAll();

        System.out.println(JSON.toJSONString(users));
    }

    @Test
    public void findAll(){
        //先设置分页
        pageHelper.startPage(2,2);
        List<SysUserEntity> users = sysUserDao.findAll();
        PageInfo<SysUserEntity> pageInfo = new PageInfo<SysUserEntity>(users);

        System.out.println(JSON.toJSONString(pageInfo));;
    }

}
