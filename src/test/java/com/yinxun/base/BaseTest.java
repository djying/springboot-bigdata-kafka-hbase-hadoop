/*
 * Copyright 2018 GXJA science technology CO., LTD. All rights reserved.
 * distributed with this file and available online at
 */
package com.yinxun.base;

import com.yinxun.HadoopApplication;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;


/***
 * <p>junit测试基类</p>
 * @ClassName BaseTest.java
 * @author    yzh_h
 * @date      2018年5月8日 上午10:13:44
 * @version   1.0
 */
@RunWith(SpringRunner.class)
@WebAppConfiguration
//@ContextConfiguration(locations={"classpath:application.yml"})
@SpringBootTest(classes = HadoopApplication.class)
public class BaseTest extends AbstractTransactionalJUnit4SpringContextTests {
}
