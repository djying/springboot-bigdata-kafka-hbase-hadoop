package com.yinxun;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.yinxun.entity.SysUserEntity;
import com.yinxun.entity.UserInfo;
import com.yinxun.dao.phoenix.UserInfoDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mortbay.util.ajax.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PhoenixTests {
    @Autowired
    private UserInfoDao userInfoMapper;

    @Autowired
    private PageHelper pageHelper;

    /**
     * 普通sql
     */
    @Test
    public void contextLoads() {
        List<UserInfo> users = userInfoMapper.findAll();

        System.out.println(JSON.toString(users));
    }

    /**
     * 分页sql
     */
    @Test
    public void findAll(){
        //先设置分页
        pageHelper.startPage(1,2);
        List<UserInfo> users = userInfoMapper.findAll();
        PageInfo<UserInfo> pageInfo = new PageInfo<UserInfo>(users);

        System.out.println(com.alibaba.fastjson.JSON.toJSONString(pageInfo));;
    }

}
