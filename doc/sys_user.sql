/*
mysql的用户表,导入对应数据库
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '用户名',
  `salt` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '盐',
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `status` tinyint(4) NULL DEFAULT NULL COMMENT '状态  0：禁用   1：正常',
  `dept_id` bigint(20) NULL DEFAULT NULL COMMENT '部门ID',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统用户' ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;


-- ----------------------------
-- 测试数据
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'admin1', 'YzcmCZNvbXocrsz9dm8e', 'test@qq.com', '13612345678', 1, 1, '2018-11-11 11:11:11');
INSERT INTO `sys_user` VALUES (2, 'admin2', 'YzcmCZNvbXocrsz9dm8e', 'test@qq.com', '13612345678', 1, 1, '2018-11-11 11:11:11');
INSERT INTO `sys_user` VALUES (3, 'admin3', 'YzcmCZNvbXocrsz9dm8e', 'test@qq.com', '13612345678', 1, 1, '2018-11-11 11:11:11');
INSERT INTO `sys_user` VALUES (4, 'admin4', 'YzcmCZNvbXocrsz9dm8e', 'test@qq.com', '13612345678', 1, 1, '2018-11-11 11:11:11');

