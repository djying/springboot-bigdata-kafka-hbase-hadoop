/**
  phoenix对应的用户表
 */
create table user(id integer primary key,name varchar);

/**
  测试数据
 */
upsert into user(id,name) values(1,'测试1');
upsert into user(id,name) values(2,'测试2');
upsert into user(id,name) values(3,'测试3');
upsert into user(id,name) values(4,'测试4');
