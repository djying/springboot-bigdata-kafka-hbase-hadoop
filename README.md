<h1>简介</h1>
    hbase 提供很方便的shell脚本以及java API等方式对Hbase进行操作，但是对于很对已经习惯了关系型数据库操作的开发来说，有一定的学习成本，如果可以像操作mysql等一样通过sql实现对Hbase的操作，那么很大程度降低了Hbase的使用成本。Apache Phoenix 组件就完成了这种需求，官方注解为 “Phoenix -we put the SQL back in NoSql”，通过官方说明，Phoenix 的性能很高，相对于 hbase 原生的scan 并不会差多少，而对于类似的组件 hive、Impala等，性能有着显著的提升，详细请阅读https://phoenix.apache.org/performance.html。

Apache Phoenix 官方站点：https://phoenix.apache.org/
Phoenix支持的sql语句： https://phoenix.apache.org/language/index.html

Phoenix 支持的DataTypes：https://phoenix.apache.org/language/datatypes.html
Phoenix 支持的函数：https://phoenix.apache.org/language/functions.html


<h1>本项目是phoenix+mysql双数据源 集成到springboot的使用</h1>

phoenix 是通过jdbc的方式连接  通过网上一些文章来看  好像druid对于phoenix 好像不太兼容 会报错,有可能是版本问题 具体没有亲自尝试,使用了HikariCP连接池
,目前是phoenix和mysql混合的项目

#1.创建数据库和导入数据
tip:前提是装有数据库,和服务器有对应的phoenix(本文对应hbase为1.2.6)
```
在doc有对应sql
1.mysql创建表并导入测试数据sys_user.sql
2.phoenix创建表并导入测试数据user_info.sql
```

<h1>2.修改自己对应的配置</h1>
在application-dev.yml等分环境配置里,分别修改mysql和phoenix配置


<h1>注意点</h1>
1.guava不能高于1.6版本,不然会报错,而swagger2.0的guava为2.0版本,所以不能用swagger2.0;
<br/>
2.好像druid对于phoenix 好像不太兼容 会报错,有可能是版本问题 ;

